<?php

class Omailo_Error extends Exception{}
class Omailo_HttpError extends Omailo_Error{}

/**
 * The provided API key is not a valid Omailo API key
 */
class Mandrill_Invalid_Key extends Mandrill_Error {}